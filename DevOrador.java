package papadev1;
import robocode.*;
import java.awt.Color;
//import robocode.WinEvent;



// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * DevOrador - a robot by (Eliezer Queiroz)
 */
public class DevOrador extends AdvancedRobot {
	int turnDirection = 1;
	boolean movingForward;
	/**
	 * run: DevOrador's default behavior
	 */
	public void run() {
		//Cores do Devorador
		setBodyColor(Color.black);
		setGunColor(Color.blue);
		setRadarColor(Color.red);
		setScanColor(Color.white);
		setBulletColor(Color.black);

		// Robot main loop
		while(true) {
		
			setTurnGunRight(360);
			setMaxVelocity(8);
			
			setTurnRight(10000 * turnDirection);
			ahead(300);
			
			
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		
			if (e.getDistance() >200 ) {
			fire(1);
		} // otherwise, fire 1.
		else if (e.getDistance() >= 100 && getEnergy() >= 70)  {
			fire(2);
		}	else if (e.getDistance() < 100 && getEnergy() >= 90)  {
			fire(3);
		}else {
			fire(1);
		}
		// Call scan again, before we turn the gun
			scan();
	
	
	}
	
	public void onHitRobot(HitRobotEvent e) {
	
		if (e.getBearing() > -10 && e.getBearing() < 10) {
			setMaxVelocity(8);
			reverseDirection();
			setTurnGunRight(360);
			setTurnGunRight( -1 * getHeading());
			fire(3);
		}
		if (e.isMyFault()) {
			//turnLeft(100);
			turnDirection = (-1 * turnDirection);
			setTurnGunRight(360);
			setMaxVelocity(8);
			reverseDirection();
			setTurnGunRight(360);
			//reverseDirection();
			//back(250);
		}
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		turnDirection = (-1 * turnDirection);
		setTurnGunRight(360);
		setMaxVelocity(8);
		reverseDirection();
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
		public void onHitWall(HitWallEvent e) {
		
		turnDirection = (-1 * turnDirection);
		setTurnGunRight(360);
		setMaxVelocity(8);
		reverseDirection();
	}
	
		public void reverseDirection() {
		if (movingForward) {
			setTurnGunRight(360);
			setTurnRight(10000);
			setBack(250);
			execute();
			movingForward = false;
		} else {
			setTurnGunRight(360);
			setTurnRight(10000);
			setAhead(250);
			execute();
			movingForward = true;
		}
	}
	
	public void onWin(WinEvent e) {
		
		for (int i = 0; i < 30; i++) {
			turnRight(30);
			turnGunLeft(30);
			turnLeft(30);
			turnGunRight(30);
	
		}
		
	}
}
