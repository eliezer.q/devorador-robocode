# DevOrador - Robocode

### Um tanque feito em Java que devora Devs

## Descrição


- **Tipo**: Advanced Robot

- **Ponto forte**: É bom contra inimigos com pouca mobilidade, se esquiva bem dos tiros

- **Ponto Fraco**: Gasta muita energia e vira alvo fácil nos cantos da arena

**Movimentação básica**: Movimento circular constante
             
    while(true) 
    {
    setMaxVelocity(8); //define velocidade maxima
	setTurnGunRight(360); //define uma volta completa da arma e do radar
    setTurnRight(10000 * turnDirection); //define movimento giratório para direita
	ahead(300); //anda pra enquanto gira para direita
    }
    
**Tática de ataque**: Rastrear o inimigo e calibrar a potêcia do tiro baseado na energia e na distancia do oponente


	public void onScannedRobot(ScannedRobotEvent e) {
		// quando encontrar um alvo atire, considerando a distancia e a energia diponível para definir a potencia do tiro
	if (e.getDistance() >200 ) {
		fire(1);
	} // otherwise, fire 1.
	else if (e.getDistance() >= 100 && getEnergy() >= 70)  {
		fire(2);
	}	else if (e.getDistance() < 100 && getEnergy() >= 90)  {
		fire(3);
	}else {
		fire(1);
	}
	// Call scan again, before we turn the gun
	scan();
	
	}



**Táticas de defesa**: Inverte o sentido do movimento rapidamente quando bate no oponente nas paredes ou recebe um tiro
	
    public void onHitByBullet(HitByBulletEvent e) { //quando recebe um tiro iverte a direção do movimento, gira a arma a procura do alvo 
		turnDirection = (-1 * turnDirection);
		setTurnGunRight(360);
		setMaxVelocity(8);
		reverseDirection();
	}

    	public void onHitWall(HitWallEvent e) { //quando bate na parede iverte a direção do movimento, gira a arma a procura do alvo 
		turnDirection = (-1 * turnDirection);
		setTurnGunRight(360);
		setMaxVelocity(8);
		reverseDirection();
	}

    	public void onHitRobot(HitRobotEvent e) { //ao receber uma batida do opnente tenta acertar um tiro nivel 3  
	
		if (e.getBearing() > -10 && e.getBearing() < 10) {
			setMaxVelocity(8);
			reverseDirection();
			setTurnGunRight(360);
			setTurnGunRight( -1 * getHeading());
			fire(3);
		}
		if (e.isMyFault()) { //ao bater no oponente iverte a direção do movimento, gira a arma a procura do alvo 
			
			turnDirection = (-1 * turnDirection);
			setTurnGunRight(360);
			setMaxVelocity(8);
			reverseDirection();
			setTurnGunRight(360);
			
		}
	}

    	public void reverseDirection() { // função que inverte a direção e executa o movimento de fuga
		if (movingForward) {
			setTurnGunRight(360);
			setTurnRight(10000);
			setBack(250);
			execute();
			movingForward = false;
		} else {
			setTurnGunRight(360);
			setTurnRight(10000);
			setAhead(250);
			execute();
			movingForward = true;
		}

		}

**Evento de vitória**: Dança quando vence
		public void onWin(WinEvent e) {
		
		for (int i = 0; i < 30; i++) {
			turnRight(30);
			turnGunLeft(30);
			turnLeft(30);
			turnGunRight(30);
	
		}
		
	}


#### Desenvolvido por **Eliezer Queiroz**